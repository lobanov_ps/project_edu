from django import forms

from ordersapp.models import MegaOrder, OrderItem
from basket.models import Product

from django.conf import settings
from django.core.cache import cache


class OrderForm(forms.ModelForm):
    class Meta:
        model = MegaOrder
        exclude = ('user',)

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class OrderItemForm(forms.ModelForm):
    price = forms.CharField(label='цена', required=False)

    class Meta:
        model = OrderItem
        exclude = ()
        # fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(OrderItemForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

        if settings.LOW_CACHE:
            key = 'orderitemform_products'
            products = cache.get(key)
            if products is None:
                products = Product.get_items().prefetch_related()
                cache.set(key, products)
        else:
            products = Product.get_items().prefetch_related()

        self.fields['product'].queryset = products
