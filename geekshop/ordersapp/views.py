from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.shortcuts import get_object_or_404, HttpResponseRedirect

from django.views.generic import CreateView, UpdateView, DeleteView, ListView
from django.views.generic.detail import DetailView
from ordersapp.models import MegaOrder, OrderItem

from django.urls import reverse, reverse_lazy
from django.db import transaction

from ordersapp.forms import OrderItemForm
from django.forms import inlineformset_factory

from basket.models import Invoice, Order, Product

from django.dispatch import receiver
from django.db.models.signals import pre_save, pre_delete

from django.http import JsonResponse

from django.db.models import F
from django.conf import settings

class OrderList(LoginRequiredMixin, ListView):
    model = MegaOrder

    def get_queryset(self):
        return MegaOrder.objects.filter(user=self.request.user)


class OrderItemsCreate(CreateView):
    model = MegaOrder
    fields = []
    success_url = reverse_lazy('ordersapp:orders_list')

    def post(self, request, *args, **kwargs):
        OrderFormSet = inlineformset_factory(MegaOrder, OrderItem, form=OrderItemForm, extra=1)
        formset = OrderFormSet(self.request.POST)
        self.formset = formset
        return super().post(request, *args, **kwargs)
    #
    def get(self, request, *args, **kwargs):

        invoice_qs = Invoice.objects.get_active().filter(users=self.request.user)
        basket_items = []
        if (invoice_qs.exists()):
            basket_items = Order.objects.filter(invoices=invoice_qs[0])

        if len(basket_items):
            OrderFormSet = inlineformset_factory(MegaOrder, OrderItem, form=OrderItemForm, extra=len(basket_items))
            self.formset = OrderFormSet()
            for num, form in enumerate(self.formset.forms):
                form.initial['product'] = basket_items[num].ware
                form.initial['quantity'] = basket_items[num].counts
                form.initial['price'] = basket_items[num].ware.prices.all()[0].value
            # self.basket_items = basket_items
        else:
            OrderFormSet = inlineformset_factory(MegaOrder, OrderItem, form=OrderItemForm, extra=1)
            self.formset = OrderFormSet()
        return super().get(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['orderitems'] = self.formset
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        orderitems = context['orderitems']

        with transaction.atomic():
            form.instance.user = self.request.user
            self.object = form.save()
            if orderitems.is_valid():
                orderitems.instance = self.object
                orderitems.save()

                invoice_qs = Invoice.objects.get_active().filter(users=self.request.user)
                if (invoice_qs.exists()):
                    basket_items = Order.objects.filter(invoices=invoice_qs[0])
                    basket_items.delete()
                    invoice_qs[0].delete()

        # удаляем пустой заказ
        if self.object.get_total_cost() == 0:
            self.object.delete()

        return super(OrderItemsCreate, self).form_valid(form)


class OrderRead(DetailView):
    model = MegaOrder

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'заказ/просмотр'
        return context



class OrderItemsUpdate(UpdateView):
    model = MegaOrder
    fields = []
    success_url = reverse_lazy('ordersapp:orders_list')

    def get_context_data(self, **kwargs):
        data = super(OrderItemsUpdate, self).get_context_data(**kwargs)
        OrderFormSet = inlineformset_factory(MegaOrder, OrderItem, form=OrderItemForm, extra=1)
        if self.request.POST:
            data['orderitems'] = OrderFormSet(self.request.POST, instance=self.object)
        else:
            queryset = self.object.orderitems.select_related()
            formset = OrderFormSet(instance=self.object, queryset=queryset)
            for form in formset.forms:
                if form.instance.pk:
                    form.initial['price'] = form.instance.product.prices.all()[0].value
            data['orderitems'] = formset
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        orderitems = context['orderitems']

        with transaction.atomic():
            self.object = form.save()
            if orderitems.is_valid():
                orderitems.instance = self.object
                orderitems.save()

        # удаляем пустой заказ
        if self.object.get_total_cost() == 0:
            self.object.delete()

        return super(OrderItemsUpdate, self).form_valid(form)


class OrderDelete(DeleteView):
    model = MegaOrder
    success_url = reverse_lazy('ordersapp:orders_list')
    # template_name = 'delete_confirm.html'

def order_forming_complete(request, pk):
    if request.method == 'POST':
        order = get_object_or_404(MegaOrder, pk=pk)
        order.status = MegaOrder.SENT_TO_PROCEED
        order.save()
        return HttpResponseRedirect(reverse('ordersapp:orders_list'))
    else:
        raise Http404


def get_product_price(request, pk):
    if request.is_ajax():
        product = Product.objects.filter(pk=int(pk)).first()
        if product:
            return JsonResponse({'price': product.prices.all()[0].value})
        else:
            return JsonResponse({'price': 0})


@receiver(pre_save, sender=OrderItem)
def product_quantity_update_save(sender, update_fields, instance, **kwargs):
    if update_fields is 'quantity' or 'product':
        if instance.pk:
            if str(type(sender.get_item(instance.pk))) != "<class 'NoneType'>":
                instance.product.quantity = F('quantity') - (instance.quantity - sender.get_item(instance.pk).quantity)
        else:
            instance.product.quantity = F('quantity') - instance.quantity
        instance.product.save()


@receiver(pre_delete, sender=OrderItem)
def product_quantity_update_delete(sender, instance, **kwargs):
    instance.product.quantity = F('quantity') + instance.quantity
    instance.product.save()
