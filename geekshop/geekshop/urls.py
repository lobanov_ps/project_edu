"""geekshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os
from django.contrib import admin
from django.urls import include, re_path
from django.conf import settings
from django.conf.urls.static import static
from accounts.views import login, MyPasswordResetDoneView, MyPasswordResetConfirmView, MyPasswordResetCompleteView
import basket.views as basket

app_name = 'geekshop'

urlpatterns = [
    re_path(r'^$', basket.MainView.as_view(), name='main'),
    re_path(r'', include('basket.urls', namespace='basket')),
    re_path(r'admin/', admin.site.urls),
    re_path(r'admin/doc/', include('django.contrib.admindocs.urls')),
    re_path(r'^ordersapp/', include('ordersapp.urls', namespace='order')),
    re_path(r'^auth/verify/google/oauth2/', include("social_django.urls", namespace="social")),
    re_path(r'auth/', include('accounts.urls', namespace='accounts')),
    re_path(r'^rosetta/', include('rosetta.urls')),
    re_path(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', MyPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    re_path(r'^accounts/password/reset/done/$', MyPasswordResetDoneView.as_view(), name='password_reset_done'),
    re_path(r'^reset/confirm/done/$', MyPasswordResetCompleteView.as_view(), name='password_reset_complete'),
    re_path(r'accounts/login/', login.as_view(), name='login'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


#if settings.DEBUG:
#    import debug_toolbar
#    urlpatterns += [re_path(r'^__debug__/', include(debug_toolbar.urls))]
