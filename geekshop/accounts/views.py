from django.contrib.auth.views import (LoginView, LogoutView, PasswordResetView, PasswordResetDoneView,
                                       PasswordResetConfirmView, PasswordResetCompleteView)
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView
from django.contrib.auth.forms import UserCreationForm
from accounts.models import User, Avatar
from accounts.forms import ShopUserRegisterForm, ShopUserEditForm, ShopUserProfileEditForm
from django.urls import reverse
from django.shortcuts import render, HttpResponseRedirect

from django.contrib import auth

from django.core.mail import send_mail
from django.conf import settings

from django.db import transaction
from django.contrib.auth.decorators import login_required

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator


def register(request):
    title = 'регистрация'

    if request.method == 'POST':
        register_form = ShopUserRegisterForm(request.POST, request.FILES)

        if register_form.is_valid():
            user = register_form.save()
            if send_verify_mail(user):
                print('сообщение для подтверждения регистрации отправлено')
                success_message = _('Сообщение для подтверждения регистрации отправлено')
                messages.success(request, success_message)
                return HttpResponseRedirect(reverse('accounts:login'))
            else:
                print('ошибка отправки сообщения для подтверждения регистрации')
                success_message = _('Ошибка отправки сообщения для подтверждения регистрации')
                messages.success(request, success_message)
                return HttpResponseRedirect(reverse('accounts:login'))
    else:
        register_form = ShopUserRegisterForm()

    content = {'title': title, 'register_form': register_form}

    return render(request, 'accounts/register.html', content)


def send_verify_mail(user):
    verify_link = reverse(
        'accounts:verify',
        args=[user.email, user.activation_key])

    title = 'Подтверждение учетной записи {}'.format(user.username)
    message = 'Для подтверждения учетной записи {0} \
    на портале {1} перейдите по ссылке: \
    \n{1}{2}'.format(user.username, settings.DOMAIN_NAME, verify_link)

    print('from: {}, to: {}'.format(settings.EMAIL_HOST_USER, user.email))
    return send_mail(
            title,
            message,
            settings.EMAIL_HOST_USER,
            [user.email],
            fail_silently=False,
        )


def verify(request, email, activation_key):
    try:
        user = User.objects.get(email=email)
        if user.activation_key == activation_key and not user.is_activation_key_expired():
            print('user {} is activated'.format(user))
            user.is_active = True
            user.save()
            auth.login(request, user)

            return render(request, 'accounts/verification.html')
        else:
            print('error activation user: {}'.format(user))
            return render(request, 'accounts/verification.html')

    except Exception as e:
        print('error activation user : {}'.format(e.args))

    return HttpResponseRedirect(reverse('basket:mainView'))

@login_required
@transaction.atomic
def edit(request):
    title = 'редактирование'

    if request.method == 'POST':
        edit_form = ShopUserEditForm(request.POST, request.FILES, instance=request.user)
        profile_form = ShopUserProfileEditForm(request.POST, instance=request.user.userprofile)
        if edit_form.is_valid() and profile_form.is_valid():
            edit_form.save()
            return HttpResponseRedirect(reverse('accounts:profile'))
    else:
        edit_form = ShopUserEditForm(instance=request.user)
        profile_form = ShopUserProfileEditForm(instance=request.user.userprofile)

    content = {'title': title, 'edit_form': edit_form, 'profile_form': profile_form}

    return render(request, 'accounts/user_form.html', content)



class MyPasswordResetView(PasswordResetView):
    template_name = 'accounts/password_reset_form.html'
    email_template_name = 'accounts/password_reset_email.html'

class MyPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'accounts/password_reset_done.html'

class MyPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'accounts/password_reset_confirm.html'

class MyPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = 'accounts/password_reset_complete.html'

class login(SuccessMessageMixin, LoginView):
    """docstring for LoginView"""
    template_name = 'accounts/login.html'
    success_message = _('Вход в систему выполнен')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginView, self).dispatch(request, *args, **kwargs)

class logout(LogoutView):
    """docstring for logoutView"""
    success_message = _('Вы вышли из системы')
    #template_name = 'registration/logged_out.html'

    def get_next_page(self):
        next_page = super().get_next_page()
        if next_page:
            messages.success(self.request, self.success_message)
        return next_page

class createProfileView(CreateView):
    """docstring for DelView"""
    model = User

class profileView(DetailView):
    """docstring for AddView"""
    model = User

    def get_object(self, queryset=None):
        return self.request.user

class changeProfileView(UpdateView):
    """docstring for AddView"""
    fields = ('username', 'first_name', 'last_name', 'email', 'description')
    form = UserCreationForm
    model = User
    success_url = '/'

    def get_object(self, queryset=None):
        return self.request.user

class deleteProfileView(DeleteView):
    """docstring for DelView"""

    model = User

class createAvatarView(CreateView):
    """docstring for DelView"""
    fields = ('image', 'title')
    model = Avatar

    def form_valid(self, form):
        form.instance.related_obj = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('accounts:profile')

class avatarView(DetailView):
    """docstring for AddView"""
    model = Avatar

class changeAvatarView(UpdateView):
    """docstring for AddView"""
    fields = ('image', 'title')
    form = UserCreationForm
    model = Avatar
    success_url = '/'

class deleteAvatarView(DeleteView):
    """docstring for DelView"""
    model = Avatar

    def get_success_url(self):
        return reverse('accounts:profile')
