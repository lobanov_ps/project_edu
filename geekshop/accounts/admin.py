from django.contrib import admin
from django.contrib.auth.models import Group, Permission
from .models import User
from basket.admin import PictureInline

# Register your models here.

class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'is_active')
    inlines = (PictureInline,)
    exclude = ('password', 'sort', 'active', 'description', 'title', 'last_login')

admin.site.register(User, UserAdmin)

admin.site.unregister(Group)
admin.site.register(Group)
admin.site.register(Permission)
