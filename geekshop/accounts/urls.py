from django.urls import re_path
from .views import (login, logout,
    createProfileView, profileView, changeProfileView, deleteProfileView,
    createAvatarView, avatarView, changeAvatarView, deleteAvatarView, MyPasswordResetView, register, verify, edit)

app_name = 'accounts'

urlpatterns = [
    re_path('login/', login.as_view(), name='login'),
    re_path('logout/', logout.as_view(), name='logout'),
    re_path('profile/', profileView.as_view(), name='profile'),
    re_path('edituserprof/', edit, name='edit'),
    re_path('deleteprofile/',  deleteProfileView.as_view(), name='deleteProfile'),
    re_path('createavatar/',  createAvatarView.as_view(), name='createAvatar'),
    re_path('delavatar/(?P<pk>\d+)/',  deleteAvatarView.as_view(), name='deleteAvatar'),
    re_path(r'password_reset', MyPasswordResetView.as_view(), name='password_reset'),
    re_path(r'^register/$', register, name='register'),
    re_path(r'^verify/(?P<email>.+)/(?P<activation_key>\w*)/$', verify, name='verify'),
]