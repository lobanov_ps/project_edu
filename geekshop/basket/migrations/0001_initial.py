# Generated by Django 2.1.1 on 2018-09-16 11:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('telephone', models.CharField(blank=True, default='-', max_length=30, verbose_name='telephone')),
                ('email', models.CharField(blank=True, default='', max_length=100, verbose_name='email')),
                ('address', models.CharField(blank=True, default='', max_length=250, verbose_name='address')),
            ],
            options={
                'verbose_name': 'Контакт',
                'verbose_name_plural': 'Контакты',
            },
        ),
        migrations.CreateModel(
            name='Core',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, default='', max_length=250, verbose_name='dc-title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='dc-description')),
                ('sort', models.IntegerField(blank=True, default=0, null=True, verbose_name='sort')),
                ('active', models.BooleanField(default=False, verbose_name='active')),
            ],
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.IntegerField(blank=True, default=0, null=True, verbose_name='value')),
                ('ye', models.CharField(default='руб.', max_length=10, verbose_name='y.e.')),
            ],
            options={
                'verbose_name': 'Цена',
                'verbose_name_plural': 'Цены',
            },
        ),
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('core_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='basket.Core')),
                ('image', models.ImageField(upload_to='images')),
            ],
            options={
                'verbose_name': 'Картинка',
                'verbose_name_plural': 'Картинки',
            },
            bases=('basket.core',),
        ),
        migrations.CreateModel(
            name='Producer',
            fields=[
                ('core_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='basket.Core')),
                ('contacts', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='basket.Contact')),
            ],
            options={
                'verbose_name': 'Производитель',
                'verbose_name_plural': 'Производители',
            },
            bases=('basket.core',),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('core_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='basket.Core')),
            ],
            options={
                'verbose_name': 'Продукт',
                'verbose_name_plural': 'Продукты',
            },
            bases=('basket.core',),
        ),
        migrations.CreateModel(
            name='ProductCategory',
            fields=[
                ('core_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='basket.Core')),
            ],
            options={
                'verbose_name': 'Продуктовая категория',
                'verbose_name_plural': 'Продуктовые категории',
            },
            bases=('basket.core',),
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='basket.ProductCategory'),
        ),
        migrations.AddField(
            model_name='picture',
            name='related_obj',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='pictures', to='basket.Core', verbose_name='pictures'),
        ),
    ]
