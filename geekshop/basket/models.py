from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db.models import Sum, F, FloatField
from django.utils.functional import cached_property

# warenkorb
# Create your models here.

class Core(models.Model):
    """ базовый класс для моделей """
    title = models.CharField(_('dc-title'), max_length=250, default='', blank=True, null=False)
    description = models.TextField(_('dc-description'), null=True, blank=True)
    sort = models.IntegerField(_(u'sort'), default=0, blank=True, null=True)
    active = models.BooleanField(_(u'active'), default=False)

    def __str__(self):
        return '{}'.format(self.title)


class Picture(Core):
    """ модель картинки """
    class Meta:
        verbose_name = _('Картинка')
        verbose_name_plural = _('Картинки')

    image = models.ImageField(upload_to='images')
    related_obj = models.ForeignKey(Core, verbose_name=_(u'pictures'), null=True, blank=True, related_name='pictures', on_delete=models.SET_NULL)

class ProductCategory(Core):
    """ модель категории продукта """
    class Meta:
        verbose_name = _('Продуктовая категория')
        verbose_name_plural = _('Продуктовые категории')
        ordering = ('sort',)

class Contact(models.Model):
    """ модель контакта """
    class Meta:
        verbose_name = _('Контакт')
        verbose_name_plural = _('Контакты')

    telephone = models.CharField(_('telephone'), max_length=30, default='-', blank=True, null=False)
    email = models.CharField(_('email'), max_length=100, default='', blank=True, null=False)
    address = models.CharField(_('address'), max_length=250, default='', blank=True, null=False)


class Producer(Core):
    """ модель производителя """
    class Meta:
        verbose_name = _('Производитель')
        verbose_name_plural = _('Производители')
        ordering = ('sort',)

    contacts = models.ForeignKey(Contact, null=True, on_delete=models.SET_NULL)

class Product(Core):
    """ модель продукта """
    class Meta:
        verbose_name = _('Продукт')
        verbose_name_plural = _('Продукты')
        ordering = ('sort',)

    category = models.ForeignKey(ProductCategory, null=True, on_delete=models.SET_NULL)
    maker = models.ForeignKey(Producer, null=True, on_delete=models.SET_NULL)
    quantity = models.PositiveIntegerField(verbose_name='количество на складе', default=0)
    price = models.DecimalField(verbose_name='цена продукта', max_digits=8, decimal_places=2, default=0)

    def get_absolute_url(self):
        return '/products/{}/'.format(self.pk)

    @staticmethod
    def get_items():
        return Product.objects.filter(active=True)


class Price(models.Model):
    """Модель цен"""

    class Meta:
        verbose_name = _('Цена')
        verbose_name_plural = _('Цены')

    product = models.ForeignKey(Product, null=True, verbose_name=_('prices'), related_name='prices', on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return '{}'.format(self.value)

class invoiceManager(models.Manager):

    def get_active(self):
        return self.filter(active=True)


class Invoice(Core):
    """ модель счёта """
    class Meta:
        verbose_name = _('Счет')
        verbose_name_plural = _('Счета')
    users = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, related_name='invoices', on_delete=models.PROTECT)
    objects = invoiceManager()

    @cached_property
    def get_cached_items(self):
        return self.orders.select_related()

    def get_total_sum(self):
        items = self.get_cached_items
        return sum(list(map(lambda x: x.counts * x.ware.prices.all()[0].value, items)))

    def get_total_counts(self):
        items = self.get_cached_items
        return sum(list(map(lambda x: x.counts, items)))

class OrderQuerySet(models.QuerySet):
    def delete(self, *args, **kwargs):
        for object in self:
            object.ware.quantity += object.counts
            object.ware.save()
        super(OrderQuerySet, self).delete(*args, **kwargs)


class orderManager(models.Manager):

    def get_active(self):
        return self.filter(active=True)

    def total_sum(self):
        return self.aggregate(summ=Sum(F('price')*F('counts'), output_field=FloatField()))['summ']

    def get_queryset(self):
        return OrderQuerySet(self.model, using=self._db)


class Order(Core):
    """ модель заказа """
    class Meta:
        verbose_name = _('Заказ')
        verbose_name_plural = _('Заказы')

    ware = models.ForeignKey(Product, null=False, related_name='orders', on_delete=models.PROTECT)
    counts = models.IntegerField()
    invoices = models.ForeignKey(Invoice, null=False, related_name='orders', on_delete=models.PROTECT)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    objects = orderManager()

    def _get_product_cost(self):
        return self.ware.prices.all()[0].value * self.counts

    product_cost = property(_get_product_cost)

    @staticmethod
    def get_item(pk):
        return Order.objects.get(pk=pk)

    def save(self, *args, **kwargs):
        if self.pk:
            self.ware.quantity = F('quantity') - (self.counts - self.__class__.get_item(self.pk).counts)
        else:
            self.ware.quantity = F('quantity') - self.counts

        self.ware.save()
        super(self.__class__, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.ware.quantity = F('quantity') + self.counts
        self.ware.save()
        super(self.__class__, self).delete(*args, **kwargs)
