from django.urls import re_path
from .views import (MainView, ContView, ProdView, ProdCategoryView,
                    ProdDetailView, AddProductView, DelProductView, UpdateProductView, OrderProductView, BasketView,
                    createPictureView, pictureView, deletePictureView, OrderDeleteProductView, OrderAddProductView,
                    PayInvoiceView, basket_edit, DeleteProductFromOrder)


app_name = 'basket'

urlpatterns = [
    re_path(r'index/', MainView.as_view(), name='mainView'),
    re_path(r'^$', MainView.as_view(), name='mainView'),
    re_path(r'products/page/(?P<page>\d+)/', ProdView.as_view(), name='prodView'),
    re_path(r'contact/', ContView.as_view(), name='contView'),
    re_path(r'products/category/(?P<cat_id>\d+)/page/(?P<page>\d+)/', ProdCategoryView.as_view(), name='prodCategoryView'),
    re_path(r'products/(?P<slug>\d+)/', ProdDetailView.as_view(), name='prodDetailView'),
    re_path(r'^addproduct/(?P<cat_id>\d+)', AddProductView.as_view(), name='addProductView'),
    re_path(r'^delproduct/(?P<pk>\d+)/', DelProductView.as_view(), name='delProductView'),
    re_path(r'edit_basket/(?P<pk>\d+)/(?P<quantity>\d+)/$', basket_edit, name='basket_edit'),
    re_path(r'delete_product_basket/(?P<pk>\d+)/$', DeleteProductFromOrder.as_view(), name='deleteProductFromOrder'),
    re_path(r'updateproduct/(?P<pk>\d+)/', UpdateProductView.as_view(), name='updateProductView'),
    re_path(r'order/(?P<pk>\d+)', OrderProductView.as_view(), name='orderProductView'),
    re_path(r'order/deleteproduct/(?P<pk>\d+)/', OrderDeleteProductView.as_view(), name='orderDeleteProductView'),
    re_path(r'order/addproduct/(?P<pk>\d+)/', OrderAddProductView.as_view(), name='orderAddProductView'),
    re_path(r'^basket/', BasketView.as_view(), name='basketView'),
    re_path(r'createPicture/(?P<related_obj>\d+)/', createPictureView.as_view(), name='createPicture'),
    re_path(r'picture/(?P<pk>\d+)/', pictureView.as_view(), name='pictureView'),
    re_path(r'deletePicture/(?P<pk>\d+)/', deletePictureView.as_view(), name='deletePicture'),
    re_path(r'payinvoice/', PayInvoiceView.as_view(), name='payinvoice')
]
