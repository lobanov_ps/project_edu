window.onload = function () {
    // добавляем ajax-обработчик для обновления количества товара
    $('.basket_list').on('change', 'input[type="number"]', function () {
        var target_href = event.target;

        if (target_href) {
            $.ajax({
                url: "/edit_basket/" + target_href.name + "/" + target_href.value + "/",

                success: function (data) {
                    $('.basket_list').html(data.result);
                    console.log('ajax done');
                },
            });

        }
        event.preventDefault();
    });
    
}