from django.contrib import admin
from .models import ProductCategory, Product, Picture, Contact, Producer, Price, Invoice, Order

# Register your models here.

class PictureInline(admin.TabularInline):
        """ настройка представления для поля картинки """
        model = Picture
        fk_name = 'related_obj'
        fields = ('title', 'active', 'image')


class ProductCategoryAdmin(admin.ModelAdmin):
        """ настройка краткого представления для категории продукта """
        list_display = ('title', 'active', 'sort')
        inlines = (PictureInline,)
        list_editable = ('sort',)

admin.site.register(ProductCategory, ProductCategoryAdmin)


class PictureAdmin(admin.ModelAdmin):
        """ настройка краткого представления для картинки """
        list_display = ('title', 'active', 'sort')
        fields = ('title', 'active', 'image', 'sort', 'description')


admin.site.register(Picture, PictureAdmin)

class PriceInline(admin.TabularInline):
        model = Price
        fk_name = 'product'
        fields = ('value',)


class ProductAdmin(admin.ModelAdmin):
        """ настройка краткого представления для продукта """
        list_display = ('title', 'active', 'sort', 'price')
        inlines = (PictureInline, PriceInline,)
        exclude = ('pictures', )
        list_editable = ('sort',)

admin.site.register(Product, ProductAdmin)

admin.site.register(Contact)

class ProducerAdmin(admin.ModelAdmin):
        """ настройка краткого представления для производителя """
        list_display = ('title', 'description', 'active', 'sort')
        list_editable = ('sort',)

admin.site.register(Producer, ProducerAdmin)

admin.site.register(Price)
admin.site.register(Invoice)
admin.site.register(Order)


