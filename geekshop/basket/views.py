from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import HttpResponseRedirect, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from .models import Product, Contact, ProductCategory, Invoice, Order, Picture
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages

from django.urls import reverse
from django.conf import settings

from django.template.loader import render_to_string
from django.http import JsonResponse

from django.core.cache import cache

from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.db import connection


def get_category(id):
    if settings.LOW_CACHE:
        key = 'category_{}'.format(id)
        category = cache.get(key)
        if category is None:
            category = get_object_or_404(ProductCategory, id=id)
            cache.set(key, category)
        return category
    else:
        return get_object_or_404(ProductCategory, id=id)


def get_product(pk):
    if settings.LOW_CACHE:
        key = f'product_{pk}'
        product = cache.get(key)
        if product is None:
            product = get_object_or_404(Product, pk=pk)
            cache.set(key, product)
        return product
    else:
        return get_object_or_404(Product, pk=pk)



class changeTemplateNameMixin(object):

    def get_template_names(self):
        old_template, new_template = settings.TEMPLATE_CHANGE
        template_names = super().get_template_names()
        if old_template != new_template:
            template_names = self.changedNamesList(template_names, old_template, new_template)
        return template_names

    @staticmethod
    def changedNamesList(template_names, old_template, new_template):
        return [name.replace(old_template, new_template) for name in template_names]

class BasketView(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'basket/order.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        invoice_qs = Invoice.objects.get_active().filter(users=self.request.user)
        if (invoice_qs.exists()):
            context['orders'] = Order.objects.filter(invoices=invoice_qs[0]).select_related()
        else:
            context['orders'] = []

        return context

class OrderProductView(ListView):
    """ Класс вывода заказа """
    model = Order
    template_name = 'basket/order.html'

    def create_new_order(self, add_invoice):
        new_order = Order()
        new_order.title = "Заказ продукта {}".format(self.kwargs['pk'])
        new_order.ware = get_product(int(self.kwargs['pk']))
        new_order.price = new_order.ware.prices.all()[0].value
        new_order.counts = 1
        new_order.invoices = add_invoice
        new_order.active = True
        new_order.save()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user_invoice = ''

        invoice_qs = Invoice.objects.get_active().filter(users=self.request.user)

        if (not invoice_qs.exists()):
            new_invoice = Invoice()
            new_invoice.title = "Счёт пользователя {}".format(self.request.user.id)
            new_invoice.users = self.request.user
            new_invoice.active = True
            new_invoice.save()
            user_invoice = new_invoice
        else:
            user_invoice = invoice_qs[0]

        order_qs = Order.objects.filter(invoices=user_invoice)

        if (not order_qs.exists()):
            self.create_new_order(user_invoice)
        else:
            is_product_added = False
            for order_entry in order_qs:
                if order_entry.ware.id == int(self.kwargs['pk']):
                    order_entry.counts += 1
                    order_entry.save()
                    is_product_added = True
            if (not is_product_added):
                self.create_new_order(user_invoice)

        context['orders'] = Order.objects.filter(invoices=user_invoice)

        return context

    def render_to_response(self, context, **response_kwargs):
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class DeleteProductFromOrder(ListView):
    model = Order
    template_name = "basket/order.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user_invoice = Invoice.objects.get_active().filter(users=self.request.user)[0]

        order_item = Order.objects.get(pk=int(self.kwargs['pk']))
        order_item.delete()
        order_qs = Order.objects.filter(invoices=user_invoice)
        if not order_qs.exists():
            user_invoice.delete()

        invoice_qs = Invoice.objects.get_active().filter(users=self.request.user)
        if invoice_qs.exists():
            context['orders'] = Order.objects.filter(invoices=invoice_qs[0])

        return context

    def render_to_response(self, context, **response_kwargs):
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))



class OrderDeleteProductView(ListView):
    model = Order
    template_name = "basket/order.html"
    success_message = _('Количество товаров было уменьшено до %s')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user_invoice = Invoice.objects.get_active().filter(users=self.request.user)[0]

        order_qs = Order.objects.filter(invoices=user_invoice)
        order_product = ''
        for order_entry in order_qs:
            if order_entry.ware.id == int(self.kwargs['pk']):
                order_product = order_entry

        order_product.counts -= 1
        if order_product.counts == 0:
            order_product.delete()
            self.success_message = ""
            order_qs = Order.objects.filter(invoices=user_invoice)
            if not order_qs.exists():
                user_invoice.delete()
        else:
            order_product.save()
            self.success_message = self.success_message % order_product.counts

        invoice_qs = Invoice.objects.get_active().filter(users=self.request.user)
        if invoice_qs.exists():
            context['orders'] = Order.objects.filter(invoices=invoice_qs[0])

        messages.success(self.request, self.success_message)
        return context

class OrderAddProductView(ListView):
    model = Order
    template_name = "basket/order.html"
    success_message = _('Количество товаров было увеличено до %s')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user_invoice = Invoice.objects.get_active().filter(users=self.request.user)[0]

        order_qs = Order.objects.filter(invoices=user_invoice)
        for order_entry in order_qs:
            if order_entry.ware.id == int(self.kwargs['pk']):
                order_entry.counts += 1
                order_entry.save()
                self.success_message = self.success_message % order_entry.counts

        context['orders'] = Order.objects.filter(invoices=user_invoice)

        messages.success(self.request, self.success_message)
        return context

class PayInvoiceView(ListView):
    model = Invoice
    template_name = "basket/order.html"
    success_message = _('Оплата прошла успешно')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        invoice_qs = Invoice.objects.get_active().filter(users=self.request.user)
        if (not invoice_qs.exists()):
            self.success_message = _('Нечего оплачивать')
        else:
            user_invoice = invoice_qs[0]
            user_invoice.active = False
            user_invoice.save()

        messages.success(self.request, self.success_message)
        return context

class MainView(changeTemplateNameMixin, ListView):
    """ Класс вывода index.html """
    model = Product
    template_name = "basket/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Product.objects.order_by('?')
        return context

class ContView(changeTemplateNameMixin, ListView):
    """ Класс вывода contact_list.html """
    model = Contact

class ProdView(changeTemplateNameMixin, ListView):
    """ Класс вывода product_list.html """
    model = Product
    paginate_by = 2

    def get_context_data(self, **kwargs):
        self.object_list = self.object_list.prefetch_related('pictures')
        context = super().get_context_data(**kwargs)
        context['category'] = ProductCategory.objects.all()
        return context

class ProdCategoryView(changeTemplateNameMixin, ListView):
    """ Класс для отображения товаров по категории """
    model = Product
    paginate_by = 1

    def get_queryset(self):
        self.category = get_category(int(self.kwargs['cat_id']))
        return Product.objects.filter(category=self.category)

    def get_context_data(self, **kwargs):
        self.object_list = self.object_list.prefetch_related('pictures')
        context = super().get_context_data(**kwargs)
        context['cat_id'] = self.category.id
        context['category'] = ProductCategory.objects.all()
        return context

class ProdDetailView(changeTemplateNameMixin, DetailView):
    """ Класс для вывода детальной информации по продукту """
    model = Product
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = ProductCategory.objects.all()
        context['cat_id'] = self.object.category.id
        return context

class AddProductView(CreateView):
    model = Product
    fields = '__all__'

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        cat_id = int(self.kwargs.get('cat_id'))
        if not cat_id == 0:
            form_kwargs['initial'] = {}
            form_kwargs['initial']['category'] = cat_id
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['name'] = "Добавить"
        return context

class DelProductView(DeleteView):
    model = Product
    success_url = '/products/'

class UpdateProductView(UpdateView):
    model = Product
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['name'] = "Изменить"
        return context

class createPictureView(CreateView):
    """docstring for DelView"""
    fields = ('image', 'title')
    model = Picture

    def form_valid(self, form):
        form.instance.related_obj_id = int(self.kwargs.get('related_obj'))
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('basket:prodDetailView',args=(self.object.related_obj.id or 0,))

    def get_context_data(self, **kwargs):
        response = super().get_context_data(**kwargs)
        response['related_obj'] = int(self.kwargs.get('related_obj'))
        return response

class pictureView(DetailView):
    """docstring for AddView"""
    model = Picture

class deletePictureView(DeleteView):
    """docstring for DelView"""
    model = Picture

    def get_success_url(self):
        return reverse('basket:prodDetailView', args=(self.object.related_obj.id or 0,) )


def basket_edit(request, pk, quantity):
    if request.is_ajax():
        user_invoice = Invoice.objects.get_active().filter(users=request.user)[0]

        quantity = int(quantity)
        new_basket_item = Order.objects.get(pk=int(pk))

        if quantity > 0:
            new_basket_item.counts = quantity
            new_basket_item.save()
        else:
            new_basket_item.delete()
            order_qs = Order.objects.filter(invoices=user_invoice)
            if not order_qs.exists():
                user_invoice.delete()
                content = {}
                result = render_to_string('basket/includes/inc_order_list.html', content)

                return JsonResponse({'result': result})

        orders = Order.objects.filter(invoices=user_invoice)

        content = {
            'invoice': user_invoice,
            'orders': orders,
        }

        result = render_to_string('basket/includes/inc_order_list.html', content)

        return JsonResponse({'result': result})


def db_profile_by_type(prefix, type, queries):
    update_queries = list(filter(lambda x: type in x['sql'], queries))
    print(f'db_profile {type} for {prefix}:')
    [print(query['sql']) for query in update_queries]


@receiver(pre_save, sender=ProductCategory)
def product_is_active_update_productcategory_save(sender, instance, **kwargs):
    if instance.pk:
        if instance.active:
            instance.product_set.update(active=True)
        else:
            instance.product_set.update(active=False)

    db_profile_by_type(sender, 'UPDATE', connection.queries)

# Create your views here.
