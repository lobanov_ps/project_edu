from basket.models import Invoice

def invoice(request):
    print('context processor basket works')
    user_invoice = []

    if request.user.is_authenticated:
        user_invoice = Invoice.objects.get_active().filter(users=request.user)
        if (user_invoice.exists()):
            user_invoice = user_invoice[0]

    return {
        'invoice': user_invoice,
    }
