from django.test import TestCase
from django.test.client import Client
from basket.models import Product, ProductCategory
from django.core.management import call_command
from django.core.exceptions import ObjectDoesNotExist


class TestMainappSmoke(TestCase):
    def setUp(self):
        call_command('flush', '--noinput')
        call_command('loaddata', 'test_db.json')

        self.client = Client()

    def test_mainapp_urls(self):
        # главная без логина
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/contact/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/products/page/1/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/products/category/20/page/1/')
        self.assertEqual(response.status_code, 200)

        for category in ProductCategory.objects.all():
            response = self.client.get(f'/products/category/{category.id}/page/1/')
            self.assertEqual(response.status_code, 200)

        for product in Product.objects.all():
            response = self.client.get(f'/products/{product.pk}/')
            self.assertEqual(response.status_code, 200)

    def tearDown(self):
        pass


class ProductsTestCase(TestCase):
    def setUp(self):
        category = ProductCategory.objects.create(title="стулья")
        self.product_1 = Product.objects.create(title="стул 1",
                                                category=category,
                                                price=1999.5,
                                                quantity=150,
                                                active=True)

        self.product_2 = Product.objects.create(title="стул 2",
                                                category=category,
                                                price=2998.1,
                                                quantity=125,
                                                active=False)

        self.product_3 = Product.objects.create(title="стул 3",
                                                category=category,
                                                price=998.1,
                                                quantity=115,
                                                active=True)

    def test_product_get(self):
        product_1 = Product.objects.get(title="стул 1")
        product_2 = Product.objects.get(title="стул 2")
        self.assertEqual(product_1, self.product_1)
        self.assertEqual(product_2, self.product_2)

    def test_product_print(self):
        product_1 = Product.objects.get(title="стул 1")
        product_2 = Product.objects.get(title="стул 2")
        self.assertEqual(str(product_1), 'стул 1')
        self.assertEqual(str(product_2), 'стул 2')

    def test_product_get_items(self):
        product_1 = Product.objects.get(title="стул 1")
        product_3 = Product.objects.get(title="стул 3")

        products = product_1.get_items()

        self.assertEqual(list(products), [product_1, product_3])

    def test_error(self):
        # Проверка ошибок
        # Проверка что будет ошибка
        with self.assertRaises(ObjectDoesNotExist):
            Product.objects.get(category__title="стулья1")

# Create your tests here.

