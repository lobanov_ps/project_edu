from django import template
register = template.Library()

@register.simple_tag
def get_sum_simple_tag(invoice):
    response = 0
    for order in invoice.orders.all():
        response += order.counts * order.price
    return response

@register.filter
def get_sum_filter(invoice):
    response = 0
    for order in invoice.orders.all():
        response += order.counts * order.price
    return response

@register.filter
def get_product_title(order):
    return order.ware.title

@register.simple_tag
def get_product_description(order):
    return order.ware.description
